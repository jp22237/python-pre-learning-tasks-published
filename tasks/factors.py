def factors(number):
    # ==============
    if number <= 0:
        return "Please choose a positive number greater than 0"
    elif number == 1:
        return [1]
    else:
        factors_list = []
        for x in range(2, number):
            if number % x == 0:
                factors_list.append(x)

    return factors_list
    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
