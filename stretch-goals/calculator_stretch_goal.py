def calculator(a, b, operator):
    # ==============
    if operator == "+":
        result = int(a + b)
    elif operator == "-":
        result = int(a - b)
    elif operator == "*":
        result = int(a * b)
    elif operator == "/":
        result = int(a / b)

    # Code to convert the result into binary
    whole = result
    # an empty list called fractional to hose the remainders
    fractional = []
    # until whole is equal to 0 carry on
    while whole != 0:
        # add the remainder of whole/2 to the list fractional
        fractional.append(whole % 2)
        # update the variable whole
        whole = int(whole / 2)

    fractional = fractional[::-1]  # reverse the order off the list
    binary = ''.join(str(e) for e in
                     fractional)  # convert the elements within the list to string and use .join to turn list into a string.
    return binary

    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
