def factors(number):
    # ==============
    factors_list = []
    count = 0
    if number == 0:
        return "Please choose a positive number greater than 0"
    elif number == 1:
        return [1]
    elif number == 2:
        return "2 is a prime number"
    for x in range(2, number):
        if number % x == 0:
            factors_list.append(x)
        else:
            for x in range(1, number + 1):
                if number % x == 0:
                    count += 1
        if count == 2:
            return str(number) + " is a prime number"

    return factors_list
    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
